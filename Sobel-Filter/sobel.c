/*
 * sobel_filter.c
 *
 *  Created on: 10.12.2015
 *      Author: Christof Schlaak
 *      Refactored: Ralf Stemmer on 16.05.2018
 */

#include <sobel.h>
#ifdef TEST
#include <stdio.h>
#endif

/*
 * Here you can select different images.
 * All headers provide the following variables with different image sizes:
	static int image_width  = 12;
	static int image_height = 12;
	static int image[12][12]= {{},..,{}};
 */

//#include <image/noise12.h>
//#include <image/noise24.h>
#include <image/noise48.h>
//#include <image/noise96.h>
//#include <image/noise256.h>
//#include <image/noise512.h>


int image_x = 0;
int image_y = 0;

void GetPixel(token_t tokensOut[])
{	
    // check if pixel available
    // If not, start from the beginning
    if(image_y >= image_height)
    {
        image_x = 0;
        image_y = 0;
    }

    int i = 0;
    int x_mask;
    int y_mask;

    for(y_mask = -4; y_mask < 5; ++y_mask)
    {
        for(x_mask = -4; x_mask < 5; ++x_mask)
        {
            int xnew;
            int ynew;

            xnew = image_x + x_mask;
            ynew = image_y + y_mask;

            if(xnew < 0)
                xnew = 0;
            if(xnew >= image_width)
                xnew = image_width - 1;
            if(ynew < 0)
                ynew = 0;
            if(ynew >= image_height)
                ynew = image_height - 1;

            tokensOut[i] = image[ynew][xnew];
            i++;
        }
    }

    image_x++;
    if(image_x >= image_width)
    {
        image_x = 0;
        image_y++;
    }
}


void GX(token_t tokensIn[], token_t tokensOut[])
{
    const int sx[81] = {
            4,  3,  2,  1,  0, -1, -2, -3, -4,
            5,  4,  3,  2,  0, -2, -3, -4, -5,
            6,  5,  4,  3,  0, -3, -4, -5, -6,
            7,  6,  5,  4,  0, -4, -5, -6, -7,
            8,  7,  6,  5,  0, -5, -6, -7, -8,
            7,  6,  5,  4,  0, -4, -5, -6, -7,
            6,  5,  4,  3,  0, -3, -4, -5, -6,
            5,  4,  3,  2,  0, -2, -3, -4, -5,
            4,  3,  2,  1,  0, -1, -2, -3, -4
        };

    tokensOut[0] = 0;

    int i;
    for(i = 0; i < 81; i++)
    {
        tokensOut[0] += tokensIn[i] * sx[81-1 - i];
    }
}


void GY(token_t tokensIn[], token_t tokensOut[])
{
    const int sy[81] = {
             4,  5,  6,  7,  8,  7,  6,  5,  4,
             3,  4,  5,  6,  7,  6,  5,  4,  3,
             2,  3,  4,  5,  6,  5,  4,  3,  2,
             1,  2,  3,  4,  5,  4,  3,  2,  1,
             0,  0,  0,  0,  0,  0,  0,  0,  0,
            -1, -2, -3, -4, -5, -4, -3, -2, -1,
            -2, -3, -4, -5, -6, -5, -4, -3, -2,
            -3, -4, -5, -6, -7, -6, -5, -4, -3,
            -4, -5, -6, -7, -8, -7, -6, -5, -4
        };

    tokensOut[0] = 0;

    int i;
    for(i = 0; i < 81; i++)
    {
        tokensOut[0] += tokensIn[i] * sy[81-1 - i];
    }
}


token_t ABS(token_t tokensIn1[], token_t tokensIn2[])
{
    if(tokensIn1[0] < 0) tokensIn1[0] = tokensIn1[0] * -1;
    if(tokensIn2[0] < 0) tokensIn2[0] = tokensIn2[0] * -1;

    int result;
    // Usually, the formular is sqrt(GX² + GY²), we hacket it this way:
    result = (tokensIn1[0] + tokensIn2[0]) & 0x000000FF;
    return (token_t) result;
}

/*
 * testing procedure only for local computer (no multi processor)
 */

#ifdef TEST
int main()
{	
	token_t source[9*9];
	token_t gx[9*9];
	token_t gy[9*9];
    token_t result;
	
	// main loop
    for(int i = 0; i < image_height*image_width; i++)
	{
		GetPixel(source);
		GX(source, gx);
		GY(source, gy);
		result = ABS(gx, gy);
        printf(" %3d", result);
        if((i+1)%image_width == 0)
            printf("\n");
	}
	
	return (0);
}
#endif

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
