# SDF Sobel-Filter

A Bare-Metal Sobel-Filter implementation following the SDF Model of Computation.

Version 1 uses a 9·9 matrix, version 2 a 3·3 matrix.
Further more version 2 uses a feedback channel to avoid an internal state in GetPixels.


