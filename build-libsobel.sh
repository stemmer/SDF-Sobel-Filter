#!/usr/bin/env bash


INSTALLPATH="./mb"
BUILDPATH="./build"
SHARED_CFLAGS="-O0 -c -mcpu=v9.6 -mlittle-endian -fdata-sections -ffunction-sections -std=c99"
GCC=mb-gcc
OBJCOPY=mb-objcopy
AR=mb-ar

for lib in libsobel.a libsobel-ea.a libsobel2.a libsobel2-ea.a
do
    if [ -f "$INSTALLPATH/$lib" ] ; then
        echo -e "\e[1;31m$lib does already exist."
        echo -e "\e[1;30mTo create a new $lib library, the old one must be removed manually."
        exit 1
    fi
done

if [ -d "$BUILDPATH" ] ; then
    echo -e "\e[1;31m$BUILDPATH does already exist."
    echo -e "\e[1;30mThe scripts expects that this directory does not exist. Please remove it manually."
    exit 1
fi

mkdir -p $BUILDPATH

# 1st argument: libname "jpeg-ef" for example
# 2nd argument: CFLAGS
function Build
{
    LIBNAME="$1"
    CFLAGS="$2"
    INCLUDEPATHS="-I$SOURCEPATH -I./mb"

    SOURCES=$(find $SOURCEPATH -type f -name "*.c")
    for s in $SOURCES ; 
    do
        echo -e -n "\e[1;34m - $s\e[0m "
        FILENAME="${s##*/}"
        OBJFILE="${FILENAME%.*}.o"
        $GCC $CFLAGS $INCLUDEPATHS -I$SOURCEPATH -c -o "$BUILDPATH/$OBJFILE" $s
        if [ $? -ne 0 ] ; then
            echo -e "\e[1;31m$GCC FAILED!\e[0m"
            exit 1
        else
            echo -e "\e[1;32m✔\e[0m"
        fi
    done

    $AR rv $INSTALLPATH/lib${LIBNAME}.a $BUILDPATH/*.o
    rm $BUILDPATH/*.o
}

mkdir -p $INSTALLPATH

# Build Libraries
SOURCEPATH="./Sobel-Filter"
Build sobel    "$SHARED_CFLAGS -mxl-soft-mul    -mxl-soft-div    -msoft-float"
Build sobel-ea "$SHARED_CFLAGS -mno-xl-soft-mul -mno-xl-soft-div -msoft-float"
SOURCEPATH="./Sobel-Filter2"
Build sobel2    "$SHARED_CFLAGS -mxl-soft-mul    -mxl-soft-div    -msoft-float"
Build sobel2-ea "$SHARED_CFLAGS -mno-xl-soft-mul -mno-xl-soft-div -msoft-float"

# Copy Headers
cp $SOURCEPATH/*.h $INSTALLPATH/.

# Make Clean
rmdir $BUILDPATH

